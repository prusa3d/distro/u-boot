### Building 32-bit SPL binary for FEL

```
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- sun50i-a64-ddr3-spl_config
make -j`nproc`
```

Resulting `sunxi-spl.bin` file can be then combined with a regular 64-bit
`u-boot.bin` binary in a command like this:

```
sunxi-fel -v -p				\
	spl sunxi-spl.bin		\
	write 0x44000 bl31.bin		\
	write 0x4a000000 u-boot.bin	\
	write 0x43100000 u-boot.env	\
	reset64 0x44000
```

The `u-boot.env` is a text-file which is read by u-boot in order to alter
environment variables. It must conform this syntax:

```
#=uEnv
variable_name=string
```

Example:

```
#=uEnv
bootcmd=ums 0 mmc 1
```
